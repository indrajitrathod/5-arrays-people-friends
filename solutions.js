const friendsList = require('./friendsData');

const convertBalanceToNumber = (friendsList) => {
    const result = friendsList.map((user) => {

        const balanceInNumber = user['balance'].replace('$', '').replaceAll(',', '');
        return { ...user, balance_in_number: balanceInNumber };
    })
        .map((user) => {
            user['balance_in_number'] = isNaN(user['balance_in_number']) ? 0 : Number(user['balance_in_number']);
            return user;
        });

    return result;
}

const sortByDescendingAge = (friendsList) => {
    const result = convertBalanceToNumber(friendsList)
        .sort((userA, userB) => {
            return userB.age - userA.age;
        });
    return result;
}

const flattenFriends = (friendsList) => {
    const result = sortByDescendingAge(friendsList).map((user) => {
        const friends = user.friends.reduce((friends, friend) => {

            return friends.concat(friend.name);
        }, []);
        user['friends'] = friends;
        return user;
    });
    return result;
}

const deleteTagsKey = (friendsList) => {
    const result = flattenFriends(friendsList).map((user) => {
        delete user["tags"];
        return user;
    });
    return result;
}

const formatRegisteredDate = (friendsList) => {
    const result = deleteTagsKey(friendsList).map((user) => {
        const newDate = user.registered.slice(0, 10).split('-').reverse().join('/');
        user.registered = newDate;
        return user;
    });
    return result;
};

const filterActiveUsers = (friendsList) => {
    const result = formatRegisteredDate(friendsList).filter((user) => {
        return user.isActive;
    });
    return result;
}

const calculateBalance = (friendsList) => {
    const result = filterActiveUsers(friendsList).reduce((totalBalance, user) => {
        return totalBalance += user.balance_in_number;
    }, 0);

    return result;
}

console.log(calculateBalance(friendsList));